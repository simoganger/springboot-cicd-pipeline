package com.simontech.springboot.cicd.pipeline;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootCiCdPipelineApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootCiCdPipelineApplication.class, args);
	}

}
